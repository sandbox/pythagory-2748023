<?php
/**
 * @file
 * The content_translation_associate module.
 */

/**
 * Implements hook_menu().
 */
function content_translation_associate_menu() {
  $items['node/%/translate/add_existing'] = array(
    'title' => 'Link Existing Translation',
    'type' => MENU_LOCAL_ACTION,
    'description' => 'Associate two existing pages as translations of each other',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('content_translation_associate_form'),
    'access arguments' => array('translate content'),
  );
  return $items;
}

/**
 * Renders both the primary form and the form confirmation.
 */
function content_translation_associate_form($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    // Render regular form.
    $form['introduction'] = array(
      '#type' => 'markup',
      '#markup' => "This form allows you to link two existing nodes as translations
      of each other. Enter the Node ID numbers of both pages below to proceed.",
    );

    // Try to determine what node we're looking at.
    $nid = '';

    if (arg(0) === 'node' && is_numeric(arg(1))) {
      $nid = arg(1);
    }

    $form['nid_one'] = array(
      '#type' => 'textfield',
      '#title' => t('First Node'),
      '#size' => 10,
      '#required' => TRUE,
      '#description' => 'Enter the Node ID of the page you want to add a translation to.',
      '#default_value' => $nid,
    );

    $form['nid_two'] = array(
      '#type' => 'textfield',
      '#title' => t('Second Node'),
      '#size' => 10,
      '#required' => TRUE,
      '#description' => 'Enter the Node ID of the translated page.',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
    );

    return $form;
  }
  else {
    // Render confirmation form.
    // Node IDs have already been checked in hook_form_validate.
    $node_one = node_load($form_state['values']['nid_one']);
    $node_two = node_load($form_state['values']['nid_two']);

    $content = 'You are linking <em>' . $node_one->title . ' (' . $node_one->language
      . ')</em> and <em>' . $node_two->title . ' (' . $node_two->language . ')</em>.
      Please review carefully before continuing.';

    $form['intro'] = array('#markup' => $content);
    $question = "Are you sure?";
    $path = current_path();

    return confirm_form($form, $question, $path);
  }
}

/**
 * Form validation.
 */
function content_translation_associate_form_validate($form, &$form_state) {
  // Only run the form validation the first time through, no data entered on confirmation.
  if (!isset($form_state['storage']['confirm'])) {
    if (!is_numeric($form_state['values']['nid_one'])) {
      form_set_error('nid_one', "Node ID must be a number.");
      return FALSE;
    }

    $node_one = node_load($form_state['values']['nid_one']);
    if (empty($node_one)) {
      form_set_error('nid_one', "Unable to load a page from Node ID One, please verify entered number.");
      return FALSE;
    }

    if ($node_one->language == LANGUAGE_NONE) {
      form_set_error('nid_one', "Node One is language-neutral and cannot be translated.");
      return FALSE;
    }

    if (!is_numeric($form_state['values']['nid_two'])) {
      form_set_error('nid_two', "Node ID must be a number.");
      return FALSE;
    }

    $node_two = node_load($form_state['values']['nid_two']);
    if (empty($node_two)) {
      form_set_error('nid_two', "Node ID Two appears to be invalid.");
      return FALSE;
    }

    if ($node_two->language == LANGUAGE_NONE) {
      form_set_error('nid_two', "Node Two is language-neutral and cannot be used as a translation.");
      return FALSE;
    }

    if ($node_one->language == $node_two->language) {
      form_set_error('nid_two', "Those nodes are the same language!");
      return FALSE;
    }

    // Ensure node one doesn't already have a translation in node_two's language.
    $node_one_translations = translation_node_get_translations($node_one->nid);
    if (array_key_exists($node_two->language, $node_one_translations)) {
      form_set_error('nid_two', "The source node already has a translation in the target's language.");
      return FALSE;
    }

    // Ensure node two doesn't already have a translation in node_one's language.
    $node_two_translations = translation_node_get_translations($node_two->nid);
    if (array_key_exists($node_one->language, $node_two_translations)) {
      form_set_error('nid_one', "The target node already has a translation in the source's language.");
      return FALSE;
    }
  }
}

/**
 * Primary form submit handler.
 */
function content_translation_associate_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == "Submit") {
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['confirm'] = TRUE;

    // Store away the node IDs for subsequent steps.
    $form_state['storage']['nid_one'] = $form_state['values']['nid_one'];
    $form_state['storage']['nid_two'] = $form_state['values']['nid_two'];
  }
  else {
    $nid_one = $form_state['storage']['nid_one'];
    $nid_two = $form_state['storage']['nid_two'];

    $result = db_update('node')
      ->fields(array(
        'tnid' => $nid_one,
      ))
      ->condition('nid', $nid_one, '=')
      ->execute();

    $result = db_update('node')
      ->fields(array(
        'tnid' => $nid_one,
      ))
      ->condition('nid', $nid_two, '=')
      ->execute();
  }
}

